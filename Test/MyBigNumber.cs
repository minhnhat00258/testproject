﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class MyBigNumber
    {
        static int[] getNumberList(string numberSequence, int maxLength)
        {
            int[] listNumber = new int[maxLength];
            for (int i = 0; i < numberSequence.Length; i++)
            {
                listNumber[maxLength - numberSequence.Length + i] = int.Parse(numberSequence[i].ToString());
            }
            return listNumber;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("number 1 : ");
            string numberOne = Console.ReadLine();
            Console.WriteLine("number 2 : ");
            string numberTwo = Console.ReadLine();

            int maxLength = numberOne.Length > numberTwo.Length ? numberOne.Length + 1 : numberTwo.Length + 1;
            int[] listNumberOne = getNumberList(numberOne, maxLength);
            int[] listNumberTwo = getNumberList(numberTwo, maxLength);

            int[] listNumberResult = new int[maxLength];

            int mind = 0;
            int result = 0;
            for (int i = maxLength - 1; i > -1; i--)
            {
                result = listNumberOne[i] + listNumberTwo[i] + mind;
                listNumberResult[i] = result % 10;
                mind = result / 10;
            }

            Console.WriteLine(string.Format("number1 : {0}", string.Join(string.Empty, listNumberOne)));
            Console.WriteLine("       +");
            Console.WriteLine(string.Format("number2 : {0}", string.Join(string.Empty, listNumberTwo)));
            Console.WriteLine("       -----------------------------------");
            Console.WriteLine(string.Format("Result  : {0}", string.Join(string.Empty, listNumberResult)));

            Console.ReadLine();

        }

    }
}
